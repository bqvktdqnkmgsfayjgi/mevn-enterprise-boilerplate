const express = require("express");
const app = express();
const path = require("path");

const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:21017/full-stack-vue");

app.use("/items", require("./routes/items"));

if (process.env.NODE_ENV === "production") {
  app.use("/dist", express.static(path.join(__dirname, "dist")));
}

if (process.env.NODE_ENV !== "test") {
  app.listen(process.env.PORT, () => {
    console.log(`Listening on port ${process.env.PORT}`);
  });
}

module.exports = app;
