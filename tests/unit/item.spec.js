const request = require("supertest");
const path = require("path");
const app = require(path.join(__dirname, "../../server"));
const { seedItems, populateItems } = require("./seed");

beforeEach(populateItems);

describe("GET /items", () => {
  it("should get all items", async () => {
    const res = await request(app)
      .get("/items")
      .expect(200);
    expect(res.body.length).toBe(seedItems.length);
  });
});
